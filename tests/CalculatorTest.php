<?php
class CalculatorTest extends \PHPUnit\Framework\TestCase
{
    public function testAddition(): void
    {
        $calculator = new App\Calculator;
        $this->assertEquals(3, $calculator->addTwoNumbers(1,2));
    }
    public function testDivision(): void
    {
        $calculator = new App\Calculator;
        $this->assertEquals(2, $calculator->divideTwoNumbers(8,4));
    }
    public function testDivision_DivideByZero_IsHandled(): void
    {
        $calculator = new App\Calculator;
        $this->expectException(InvalidArgumentException::class);
        $calculator->divideTwoNumbers(4,0);
    }
}