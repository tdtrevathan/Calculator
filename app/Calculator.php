<?php

namespace App;

final class Calculator
{
    public function addTwoNumbers($number1, $number2) 
    {
        return $number1 + $number2;
    }

    public function divideTwoNumbers($number1, $number2) 
    {
        if($number2 == 0) throw new \InvalidArgumentException("Cannot Divide by Zero");
        return $number1 / $number2;
    }
}
